import unittest
from chiffrecesar import *
from affichagechiffrecesar import *

class TestChiffreCesar(unittest.TestCase):

    def test_encoder(self):
        self.assertEqual(encoder("abcd",4),"efgh")
        self.assertEqual(encoder("bonjour" ,8),"jwvrw}z")
    
    def test_Decoder(self):
        self.assertEqual(decoder("efgh", 4),"abcd")
        self.assertEqual(decoder("Bonjour",8),":gfbgmj")
    
    def test_encoder_circulaire(self):
        self.assertEqual(encoder_circulaire("abc", 3),"def")
        self.assertEqual(encoder_circulaire("xyza",4),"bcde")
    
    def test_decoder_circulaire(self):
        self.assertEqual(decoder_circulaire("xyz",8),"pqr")
        self.assertEqual(decoder_circulaire("bonjour",12), "pcbxcif")