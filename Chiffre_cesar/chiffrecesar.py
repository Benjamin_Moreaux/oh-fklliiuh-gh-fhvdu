def encoder (liste, decalage):
    stringRetour = ''
    for character in liste:
        if ((ord(character) != 32) & (ord(character) != 10)):
            nouveaucharactere = chr(ord(character)+decalage)
            stringRetour = ''.join([stringRetour,nouveaucharactere])
        else: 
            nouveaucharactere = chr(ord(character))
            stringRetour= ''.join([stringRetour,nouveaucharactere])
    return stringRetour

def decoder (liste, decalage):
    stringRetour = ''
    for character in liste:
        if ((ord(character) != 32) & (ord(character) != 10)):
            nouveaucharactere = chr(ord(character)-decalage)
            stringRetour = ''.join([stringRetour,nouveaucharactere])
        else: 
            nouveaucharactere = chr(ord(character))
            stringRetour= ''.join([stringRetour,nouveaucharactere])
    return stringRetour

def encoder_circulaire(liste, decalage):
    stringRetour = ''
    for character in liste:
        if ((ord(character) != 32) & (ord(character) != 10)):
            if( ((ord(character) > 97 & ord(character) <= 122)) & ((ord(character)+decalage) > 122) | ((ord(character) > 65) & (ord(character) <= 90)) & (ord(character)+decalage > 90)):
                nouveaucharactere = chr(ord(character)+decalage-26)
                stringRetour = ''.join([stringRetour,nouveaucharactere])
            else:
                nouveaucharactere = chr(ord(character)+decalage)
                stringRetour = ''.join([stringRetour,nouveaucharactere])
        else:
            nouveaucharactere = chr(ord(character))
            stringRetour= ''.join([stringRetour,nouveaucharactere])     
    return stringRetour

def decoder_circulaire(liste, decalage):
    stringRetour = ''
    for character in liste:
        if ((ord(character) != 32) & (ord(character) != 10)):
            if((ord(character) >= 97 & ord(character) <= 122) & ((ord(character)-decalage) < 97) | ((ord(character) >= 65) & (ord(character) <= 90)) & (ord(character)-decalage < 65)):
                nouveaucharactere = chr(ord(character)-decalage+26)
                stringRetour = ''.join([stringRetour,nouveaucharactere])
            else:
                nouveaucharactere = chr(ord(character)-decalage)
                stringRetour = ''.join([stringRetour,nouveaucharactere])
        else:
            nouveaucharactere = chr(ord(character))
            stringRetour= ''.join([stringRetour,nouveaucharactere])     
    return stringRetour