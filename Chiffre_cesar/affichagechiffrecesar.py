from chiffrecesar import *
from tkinter import *

frame = Tk()

frame.title("Chiffre César")
stringRetour ='Le retour de votre text se fera ici'

textareaUn = Text(frame, wrap='word') # création 
textareaUn.insert(END, "Entrez votre text ici ") # insertion d'un texte
chaine_saisie = textareaUn.get("0.0", END) # récupération du texte
textareaUn.grid(row=1,column=1)

textareaDeux = Text(frame, wrap='word') # création du composant
textareaDeux.insert(END, stringRetour) # insertion d'un texte
textareaDeux.get("0.0", END) # récupération de l'ensemble du texte
textareaDeux.grid(row=1,column=2)


def affichage_encoder():
    saisie = textareaUn.get("0.0", END).strip() # récupération de l'ensemble du texte
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, encoder(saisie,decalage.get())) # insertion d'un texte

def affichage_decoder():
    saisie = textareaUn.get("0.0", END).strip() # récupération de l'ensemble du texte
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, decoder(saisie,decalage.get())) # insertion d'un texte


bouton_Encoder = Button(frame, text = "Encoder", command = lambda:affichage_encoder())
bouton_Encoder.grid(row=2,column=1)


bouton_Decoder = Button(frame, text = "Decoder", command = lambda:affichage_decoder())
bouton_Decoder.grid(row=2,column=2)


def affichage_encoder_circulaire():
    saisie = textareaUn.get("0.0", END).strip() # récupération de l'ensemble du texte
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, encoder_circulaire(saisie,decalage.get())) # insertion d'un texte

def affichage_decoder_circulaire():
    saisie = textareaUn.get("0.0", END).strip() # récupération de l'ensemble du texte
    textareaDeux.delete("0.0", END)
    textareaDeux.insert(END, decoder_circulaire(saisie,decalage.get())) # insertion d'un texte

bouton_Encoder_Circulaire = Button(frame, text = "Encoder circulairement", command = lambda:affichage_encoder_circulaire())
bouton_Encoder_Circulaire.grid(row=3,column=1)

bouton_Decoder_Circulaire = Button(frame, text = "Decoder circulairement", command = lambda:affichage_decoder_circulaire())
bouton_Decoder_Circulaire.grid(row=3,column=2)

decalage = IntVar()
decalageLbl = Label(frame, text="Décalage :")
decalageLbl.grid(column = 1, row = 4)
SaisieDecalage = Entry(frame, textvariable = decalage)
SaisieDecalage.grid(column = 2, row = 4)

frame.mainloop()